import scipy.signal as signal
from scipy.signal import savgol_filter
import matplotlib.pyplot as plt
from time import process_time

smooth_waves = []

def accept_wave():
    global smooth_waves
    wave = []
    while True:
        a = float(input())
        if a == -10:
            input_taken = False
            wave = savgol_filter(wave, 101, 4)
            smooth_waves.append(wave)
            return wave, input_taken
        if a == -20:
            input_taken = True
            wave = savgol_filter(wave, 101, 4)
            smooth_waves.append(wave)
            return wave, input_taken
        wave.append(a)
        
def harmonic_mean(sample):
    a = 0
    for i in sample:
        a += 1 / i
    return (len(sample) / a)

def distance_between_waves(wave_means):
    cost_mat = []
    for j in wave_means:
        cost = 0
        for i in wave_means:
            cost += ((j - i)**2)**0.5
        cost_mat.append(cost / len(wave_means))
    return cost_mat

def sort_by_means(labels, costs):
    for i in range(len(costs)):
        j = i + 1
        for j in range(len(costs)):
            if costs[i] < costs[j]:
                tmp = costs[i]
                costs[i] = costs[j]
                costs[j] = tmp
                tmpL = labels[i]
                labels[i] = labels[j]
                labels[j] = tmpL
    print('\nsorted average costs and labels:')
    for i, j in zip(costs, labels):
        print(i, '\t', j)
    return labels[0]

def main():
    input_taken = False
    wave = []
    mean_mat = []
    wave_mat = []
    label_mat = []
    cost_mat = []
    harmonic_time = {}
    iterator = 0
    while True:
        print('enter wave')
        wave, input_taken = accept_wave()
        t0_start = process_time()
        mean_mat.append(harmonic_mean(wave))
        t0_stop = process_time()
        a = len(wave)
        harmonic_time.update({a:t0_stop - t0_start})
        wave_mat.append(wave)
        label_mat.append(iterator)
        iterator += 1
        if input_taken: break
    t1_start = process_time()
    cost_mat = distance_between_waves(mean_mat)
    top_label = sort_by_means(label_mat, cost_mat)
    t1_stop = process_time()
    print('=============================================================')
    print('>>>time taken for calculating means:')
    print('length\ttime')
    for i, j in harmonic_time.items(): print(i, j,sep='\t')
    print('\n>>>time taken for getting medoid:')
    print(t1_stop - t1_start)
    print('=============================================================')
    print('\n>>>calculation over', iterator, 'waves')
    print('\n>>>medoid wave:')
    for i in wave_mat[top_label]:
        print(i)
    print()
    print('>>>length of medoid wave:', len(wave_mat[top_label]))
    print('smooth waves:')
    for i in range(len(smooth_waves)):
        print(*smooth_waves[i], sep='\t')
    for s in wave_mat:
        plt.plot(range(0, len(s)), s)
    plt.draw()
    plt.figure()
    plt.plot(range(0, len(wave_mat[top_label])), wave_mat[top_label])
    plt.show()

main()
